import { EventEmitter } from '@angular/core';
import { Ingredients } from '../app/share/ingredients.model';
import { Recipe } from '../models/recipe.model';

export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();
  recipeList = new EventEmitter<Recipe[]>();
  recipes: Recipe[] = [
    new Recipe(
      'Burger',
      'Burger For breakface',
      'https://pantograph0.goldbely.com/cfill-h630-w1200/uploads/merchant/main_image/559/hancock-gourmet-lobster-co.c62365d58493722415029905459b0cc6.jpg',
      [
        new Ingredients('sala', 2),
        new Ingredients('Meat', 1),
        new Ingredients('Cheese', 2)
      ]
    ),

    new Recipe(
      'Taro',
      'traditional Food Bamileke',
      'https://www.rotinrice.com/wp-content/uploads/2011/01/IMG_8026.jpg',
      [
        new Ingredients('Palm Oil', 2),
        new Ingredients('Taro', 20),
        new Ingredients('Meat', 2)
      ]
    ),
    new Recipe(
      'Okok',
      'very tasty south cameroon food',
      'https://i0.wp.com/shapeupafrican.com/wp-content/uploads/2017/02/erunew-2.jpg?resize=1024%2C683',
      [
        new Ingredients("Feuille d'okok ", 42),
        new Ingredients("Pate d'arachide", 5),
        new Ingredients('Bifaga', 6)
      ]
    )
  ];

  onRecipeSelected(recipe: Recipe) {
    this.recipeSelected.emit(recipe);
  }

  getRecipe(id: number) {
    if (id <= this.recipes.length && id >= 0) {
      return this.recipes[id];
    }
    return null;
  }

  getRecipes() {
    return this.recipes.slice();
    //  this.recipeList.emit(this.recipes.slice());
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipeList.emit(this.recipes.slice());
  }

  updateRecipe(id: number, recipe: Recipe) {
    if (this.recipes[id]) {
      this.recipes[id] = recipe;
      this.recipeList.emit(this.recipes.slice());
    } else {
      this.addRecipe(recipe);
    }
  }
  deleteRecipe(id: number) {
    this.recipes.splice(id, 1);
    this.recipeList.emit(this.recipes.slice());
  }
}
