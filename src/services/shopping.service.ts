import {EventEmitter} from '@angular/core';
import {Subject} from 'rxjs/Subject';

import {Ingredients} from '../app/share/ingredients.model'

export class ShoppingServices {

  ingredientAdded  = new EventEmitter<Ingredients[]>();
  ingredientWasSelected = new Subject<number>()

  private  ingredients :Ingredients[] =[
      new Ingredients("Taro",3),
      new Ingredients("Okok",2),
      new Ingredients("Palm oil",1),
    ];

    getIngredients(){
      return this.ingredients.slice()
    }
    getIngredient(id:number){
      return this.ingredients[id];
    }

    addIngredients(ingredient:Ingredients){
      this.ingredients.push(ingredient);
      this.ingredientAdded.emit(this.ingredients.slice());
    }
    addListIngredients(list:Ingredients[]){

    }

    selectedIngredient(id:number){
      this.ingredientWasSelected.next(id);
    }

    updateIngredient(id:number,ingredient:Ingredients){
      if(this.ingredients[id]){
        this.ingredients[id].name=ingredient.name;
        this.ingredients[id].amount=ingredient.amount;
      }
    }

    deleteIngredient(id:number){
      this.ingredients.splice(id,1);
      this.ingredientAdded.emit(this.ingredients.slice());
    }
}
