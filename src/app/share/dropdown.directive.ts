import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  // add a listener to attache the class "open" to the selected element
  @HostBinding('class.open') isOpen = false;
  // add a listener to the click event on the selected element
  @HostListener('click') toggleOpen() {
    this.isOpen = !this.isOpen;
  }
}
