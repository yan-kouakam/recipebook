import {Component,
          OnInit, ViewChild,
          OnDestroy} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Subscription} from 'rxjs/Subscription';

import {Ingredients} from "../../share/ingredients.model";
import {ShoppingServices} from '../../../services/shopping.service';
@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit,OnDestroy {
 @ViewChild('f') formItem:NgForm;
  id:number;
  editMode:boolean=false;
    subscription:Subscription;
    constructor(private shoppingSrv:ShoppingServices ) { }

    ngOnInit() {
      this.subscription= this.shoppingSrv.ingredientWasSelected
       .subscribe((ind:number)=>{
         this.id=ind;
         this.editMode=true;
         const ingredient =this.shoppingSrv.getIngredient(ind);
         this.formItem.setValue({
           ingredient_name:ingredient.name,
           ingredient_amount:ingredient.amount
         });
         // this.formItem.amount.setValue=ingredient.amount;

       });
    }

    ngOnDestroy(){
      this.subscription.unsubscribe();
    }

    resetForm(){
    this.formItem.reset();
    this.editMode=false;
    }


    addIngredient(f:NgForm){
      const name=f.value.ingredient_name
      const amount=f.value.ingredient_amount
      const ingr= new Ingredients(name,
                            amount);
      if(this.editMode){
      this.shoppingSrv.updateIngredient(this.id,ingr)
        this.editMode=false;

      }else{
            this.shoppingSrv.addIngredients(ingr);
           }
           this.resetForm();
    }
    onDelete(){
      this.shoppingSrv.deleteIngredient(this.id);
      this.resetForm();
    }

}
