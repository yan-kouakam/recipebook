import { Component, OnInit } from '@angular/core';
import {Ingredients} from "../share/ingredients.model";
import {ShoppingServices} from '../../services/shopping.service';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.css']
})
export class ShoppingComponent implements OnInit {
ingredients :Ingredients[]=[];


  constructor(private shoppingSrv:ShoppingServices) { }

  ngOnInit() {
    this.ingredients=this.shoppingSrv.getIngredients();
    this.shoppingSrv.ingredientAdded.subscribe(
      (ings:Ingredients[])=>this.ingredients=ings
    );
  }

 onEditItem(id:number){
   this.shoppingSrv.selectedIngredient(id);
 }

}
