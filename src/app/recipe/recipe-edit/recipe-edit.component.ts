import { Component, OnInit } from '@angular/core';
import {NgForm, Validators,
        FormGroup, FormArray,
        FormControl} from '@angular/forms';
import {Router,ActivatedRoute,Params} from '@angular/router';


import {RecipeService} from '../../../services/recipe.services';
import {Recipe} from '../../../models/recipe.model';
import {Ingredients} from '../../share/ingredients.model';


@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
editMode=false;
id :number;
recipe:Recipe;
formRecipe:FormGroup;
ingredientForm:FormArray =new FormArray([]);

  constructor( private router:Router,private route:ActivatedRoute,
               private recipeService:RecipeService) { }

  ngOnInit() {

    this.route.params
    .subscribe((params:Params)=>{
      if (params['id']){
        this.editMode = true;
        this.id=+params['id'];
      }
        this.initForm();
    });

  }

  addIngredient(){
    let newIngregientForm = new FormGroup({
      'name':new FormControl("",Validators.required),
      'amount':new FormControl("",[
        Validators.required,
        Validators.pattern(/^[1-9]+[0-9]*/)
      ])
    });
    this.ingredientForm.push(newIngregientForm);
  }
  private initForm(){
    let name='test';    let img= '';
    let desc='';    let ingredients=null;


    if (this.editMode){

    let recipe = this.recipeService.getRecipe(this.id);
    this.recipe =recipe;
        name= recipe.name;
        img = recipe.imagePath;
        desc =recipe.description;

        if(recipe['ingredients']){
          for (let ingredient of recipe['ingredients']){
            this.ingredientForm.push(new FormGroup({
              'name':new FormControl(ingredient.name,Validators.required),
              'amount':new FormControl(ingredient.amount,[
                Validators.required,
                Validators.pattern(/^[1-9]+[0-9]*/)
              ])
            })
          );
          }
        }
    }
      this.formRecipe = new FormGroup({
        'name': new FormControl(name ,Validators.required),
        'description': new FormControl(desc,Validators.required),
        'imagePath': new FormControl(img,Validators.required),
        'ingredients':this.ingredientForm
      });

  }


onSave(){
  console.log(this.formRecipe);
  let recipe = this.formRecipe.value;
  // new Recipe(name:this.formRecipe.value['name'],
  //                         description::this.formRecipe.value['name'],
  //                         imgPath:this.formRecipe.value['imgPath'],
  //                         ingredients:this.formRecipe.value['ingredients']);

     if (this.editMode){
        this.recipeService.updateRecipe(this.id ,recipe);

     } else{
       this.recipeService.addRecipe(recipe);
     }
      this.router.navigate(["/recipe"]);
}

onCancel(){
  this.formRecipe.reset();
    this.router.navigate(["/recipe"]);
}

}
