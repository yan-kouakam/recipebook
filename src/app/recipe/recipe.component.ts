import { Component, OnInit } from '@angular/core';
import { Recipe } from '../../models/recipe.model';
import { RecipeService } from '../../services/recipe.services';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit {
  selectedRecipe: Recipe = null;
  constructor(private recipeSrv: RecipeService) {}

  ngOnInit() {
    this.recipeSrv.recipeSelected.subscribe(
      (r: Recipe) => (this.selectedRecipe = r)
    );
  }
}
