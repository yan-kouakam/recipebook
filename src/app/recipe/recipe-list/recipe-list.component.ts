import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  OnDestroy
} from '@angular/core';
import { Router } from '@angular/router';
import { Recipe } from '../../../models/recipe.model';
import { RecipeService } from '../../../services/recipe.services';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {
  recipes: Recipe[];
  constructor(private recipeSrv: RecipeService, private router: Router) {}

  ngOnInit() {
    this.recipes = this.recipeSrv.getRecipes();

    this.recipeSrv.recipeList.subscribe((recipes: Recipe[]) => {
      this.recipes = recipes;
    });
  }

  ngOnDestroy() {
    //  this.subscription.unsubscribe();
  }

  recipeWasSelected(recipe: Recipe) {
    this.recipeSrv.onRecipeSelected(recipe);
  }

  onCreate() {
    this.router.navigate(['/recipe', 'new']);
  }
}
