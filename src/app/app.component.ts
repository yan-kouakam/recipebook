import { Component } from '@angular/core';

import {HeaderComponent} from "./header/header.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showRecipe:string='recipe';

  selectItem(item:string){
    this.showRecipe=item;
  }
}
