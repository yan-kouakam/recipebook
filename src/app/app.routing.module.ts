import {Routes,RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';

import {RecipeComponent} from './recipe/recipe.component';
import {ShoppingComponent} from './shopping/shopping.component';
import {RecipeItemComponent} from './recipe/recipe-list/recipe-item/recipe-item.component';
import {ShoppingListEditComponent} from './shopping/shopping-list-edit/shopping-list-edit.component'
import {RecipeDetailComponent}   from './recipe/recipe-detail/recipe-detail.component';
import {RecipeStartComponent} from './recipe/recipe-start/recipe-start.component';
import {RecipeEditComponent} from './recipe/recipe-edit/recipe-edit.component';


const routes :Routes=[
  {path:'',redirectTo:'recipe',pathMatch:'full'},
  {path:'recipe',component : RecipeComponent,
    children:[
      {path:'',component:RecipeStartComponent},
      {path:'new',component:RecipeEditComponent},
      {path:':id',component:RecipeDetailComponent},
      {path:':id/edit',component:RecipeEditComponent}
    ]},
  {path : 'shopping-list' , component:ShoppingComponent,
     children:[
       {path:':id/edit' ,component:ShoppingListEditComponent},
     ]}
]


@NgModule({
  imports:[
   RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule{

}
